package br.com.itau.porta.controllers;

import br.com.itau.porta.dtos.PortaDTOEntradaPost;
import br.com.itau.porta.models.Porta;
import br.com.itau.porta.services.PortaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/porta")
public class PortaController {

    @Autowired
    private PortaService portaService;

    @PostMapping
    public ResponseEntity<Porta> postPorta(@RequestBody PortaDTOEntradaPost portaDTOEntradaPost) {
        Porta porta = new Porta();
        porta.setAndar(portaDTOEntradaPost.getAndar());
        porta.setSala(portaDTOEntradaPost.getSala());

        Porta portaObjeto = portaService.salvarPorta(porta);

        return ResponseEntity.status(201).body(portaObjeto);
    }

    @GetMapping("/{id}")
    public Porta getPortaById(@PathVariable(name = "id") Long id) {
        try {
            Porta portaObjeto = portaService.consultarPortaPorId(id);
            return portaObjeto;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

    }
}
