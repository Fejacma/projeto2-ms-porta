package br.com.itau.porta.dtos;

public class PortaDTOEntradaPost {

    private long andar;
    private String sala;

    public PortaDTOEntradaPost() {
    }

    public PortaDTOEntradaPost(long andar, String sala) {
        this.andar = andar;
        this.sala = sala;
    }

    public long getAndar() {
        return andar;
    }

    public void setAndar(long andar) {
        this.andar = andar;
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }
}
