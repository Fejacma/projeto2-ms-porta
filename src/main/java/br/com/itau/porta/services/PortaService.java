package br.com.itau.porta.services;

import br.com.itau.porta.models.Porta;
import br.com.itau.porta.repositories.PortaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PortaService {

    @Autowired
    private PortaRepository portaRepository;

    public Porta salvarPorta(Porta porta) {

        Porta portaObjeto = portaRepository.save(porta);
            return portaObjeto;
        }


    public Porta consultarPortaPorId(Long id) {
        Optional<Porta> portaOptional = portaRepository.findById(id);

        if (portaOptional.isPresent()){
            return portaOptional.get();
        }
        throw new RuntimeException("Porta não encontrada.");
    }

}
