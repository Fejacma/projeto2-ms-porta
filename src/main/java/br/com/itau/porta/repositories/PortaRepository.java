package br.com.itau.porta.repositories;

import br.com.itau.porta.models.Porta;
import org.springframework.data.repository.CrudRepository;

public interface PortaRepository extends CrudRepository<Porta, Long> {

//    Iterable<Porta> findAllById(long id);

}
